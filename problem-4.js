function calcDivisor(n) {
    var divisor = [];
    var remainder;
    var divisorStr;

    for (var i = 0; i <= n; i++) {
        remainder = n % i;
        if (remainder === 0) {
            divisor.push(i);
        }
    }

    divisorStr = divisor.join(", "); 

    console.log(`Ước số của ${n} là: ${divisorStr}`);
}

calcDivisor(16);
