function reverseCharacter(number) {
    var reverseNumber;
    var numStr = number.toString();
    var numArr = [];
    var temp;

    if (number < 0) {
        console.log("Hãy nhập vào số nguyên dương");
        return;
    } else if (number !== parseInt(number)) {
        console.log("Hãy nhập vào số nguyên dương");
        return;
    }

    for (var i = 0; i < numStr.length; i++) {
      numArr.push(numStr[i]);
    }

    for (var i = 0; i < (numArr.length / 2); i++) {
        temp = numArr[i];
        numArr[i] = numArr[(numArr.length - 1) - i]
        numArr[(numArr.length - 1) - i] = temp;
    }

    reverseNumber = parseInt(numArr.join(""));

    console.log(reverseNumber);
}

reverseCharacter(1234567);

