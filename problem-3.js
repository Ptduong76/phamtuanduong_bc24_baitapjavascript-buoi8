function calcSum(n) {
    var tempSum = 0;
    var totalSum;

    if (n === 1) {
        console.log("n phải lớn hơn 1");
        return;
    }

    for (var i = 2; i <= n; i++) {
        tempSum += i;
    }

    totalSum = tempSum + (2 * n);

    console.log(totalSum);
}

calcSum(10);