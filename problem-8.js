var players = [[], [], [], []];

function splitCards(arr) {
    for (var i = 0; i < arr.length; i++) {
        if (i % 4 === 0) {
            players[0].push(arr[i]);
        } else if (i % 4 === 1) {
            players[1].push(arr[i]);
        } else if (i % 4 === 2) {
            players[2].push(arr[i]);
        } else if (i % 4 === 3) {
            players[3].push(arr[i]);
        }
    }

    console.log(players[0], players[1], players[2], players[3]);
}

splitCards(["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S", 
"AS", "7H", "9K", "10D"]);
