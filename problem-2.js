function findPrimeNumbers(arr) {
  var primeArr = [];
  var divide;
  var primeNum;

  for (var i = 0; i < arr.length; i++) {
    if (arr[i] === 1 || arr[i] === 2 || arr[i] === 3) {
      primeNum = true;
    }

    for (var k = 2; k <= Math.sqrt(arr[i]); k++) {
      divide = arr[i] % k;
      if (divide === 0) {
        primeNum = false;
        break;
      } else {
        primeNum = true;
      }
    }

    if (primeNum) {
      primeArr.push(arr[i]);
    }
  }

  console.log(primeArr);
}

findPrimeNumbers([1, 2, 5, 8, 12, 22, 49, 37, 121, 53, 131]);
