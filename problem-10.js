function calcDegree(hour, minute) {
  var degOneMin = 360 / 60;
  var deg;
  var hourToMinute;

  if (hour >= 24) {
    console.log("Hãy nhập số giờ hợp lệ");
    return;
  } else if (minute >= 60) {
    console.log("Hãy nhập số phút hợp lệ");
    return;
  }

  switch (hour) {
    case 12:
      hour = 0;
      break;
    case 13:
      hour = 1;
      break;
    case 14:
      hour = 2;
      break;
    case 15:
      hour = 3;
      break;
    case 16:
      hour = 4;
      break;
    case 17:
      hour = 5;
      break;
    case 18:
      hour = 6;
      break;
    case 19:
      hour = 7;
      break;
    case 20:
      hour = 8;
      break;
    case 21:
      hour = 9;
      break;
    case 22:
      hour = 10;
      break;
    case 23:
      hour = 11;
      break;
    default:
      break;
  }

  hourToMinute = hour * 5 + (minute / 60) * 5;

  if (Math.abs(hourToMinute - minute) <= 30) {
    deg = Math.abs(hourToMinute - minute) * degOneMin;
  } else {
    deg = (Math.abs(hourToMinute - minute) - 30) * degOneMin;
  }

  console.log("Góc lệch giữa kim giờ và kim phút là: " + deg + " độ");
}

calcDegree(13, 00);
